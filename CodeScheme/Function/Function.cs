// /home/jeremie/TaoParser/NGenerator/CodeScheme/Function.cs
// Writtent by jeremie at 19:22 10/06/2007
//
// This file is licensed under the LGPL licence as described in the COPYING file

using System;

namespace CodeScheme
{
	public abstract class Function: ISymbol
	{
		Argument[] args;
		string name;
		Type returnType;
	}
}
