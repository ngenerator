// /home/jeremie/TaoParser/NGenerator/CodeScheme/IFunctionFactory.cs
// Writtent by jeremie at 19:19 10/06/2007
//
// This file is licensed under the LGPL licence as described in the COPYING file

using System;

namespace CodeScheme
{
	public interface IFunctionFactory
	{
		WrapperFunction GetWrapperFunction(PInvokeFunction func);
		PInvokeFunction GetPInvokeFunction(string name, Type returnType, Argument[] args);
	}
}
