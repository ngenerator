// /home/jeremie/TaoParser/NGenerator/CodeScheme/RootObject.cs
// Writtent by jeremie at 19:19 10/06/2007
//
// This file is licensed under the LGPL licence as described in the COPYING file

using System;
using System.Collections.Generic;

namespace CodeScheme
{
	public sealed class RootObject
	{
		List<Type> typeList = new List<CodeScheme.Type>();
		List<Function> functionList = new List<CodeScheme.Function>();
	}
}
