// /home/jeremie/TaoParser/NGenerator/Asserter.cs
// Writtent by jeremie at 09:46 08/06/2007
//
// This file is licensed under the LGPL licence as described in the COPYING file

using System;

namespace NGenerator
{
	public static class Asserter
	{
		public static void Assert(bool condition)
		{
			Assert(condition, string.Empty);
		}
		
		public static void Assert(bool condition, string message)
		{
			if (condition) return;
			
			Console.ForegroundColor = ConsoleColor.DarkRed;
			Console.WriteLine("Bad assertion (" + message + "). Result is " + condition.ToString());
			Console.ResetColor();
		}
	}
}
