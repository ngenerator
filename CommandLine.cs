// /home/jeremie/TaoParser/NGenerator/CommandLine.cs
// Writtent by jeremie at 21:43 03/06/2007
//
// This file is licensed under the LGPL licence as described in the COPYING file

using System;
using System.Collections.Generic;

namespace NGenerator
{
	public class CommandLine
	{
		string[]    args;
		IDictionary<string, string> options;
		//protected Option[]  options;
		string[]  names;
		char      argSeparator = ':';
		char      argId        = '-';
		int       nameIndex    = -1;
		
		public CommandLine(string[] argLine)
		{
            if (argLine == null)
                throw new ArgumentNullException("argLine");

			args = new string[argLine.Length];
			argLine.CopyTo(args, 0);
		}
		
		public CommandLine(string[] argLine, char argument, char separator) : 
			this(argLine)
		{
			argSeparator = separator;
			argId        = argument;
		}
		
		/*public Option[] Options {
			get {
				if (options == null)
					ParseOptions();
				return options;
			}
		}
		*/
		
		public string[] Names {
			get {
				if (names == null)
					ParseNames();
                string[] namesTemp = new string[names.Length];
                Array.Copy(names, namesTemp, names.Length);
				return namesTemp;
			}
		}
		
		public bool IsDefined(string option)
		{
			if (options == null)
				ParseOptions();
			return options.ContainsKey(option);
		}
		
		public string this[string key] {
			get {
				string output = null;
				options.TryGetValue(key, out output);
				return output;
			}
		}
		
		protected void ParseOptions()
		{
			if (options != null)
				return;
				
			options = new Dictionary<string, string>();
			
			int index = -1;
			
			foreach (string s in args) {
				if (s[0] == argId) {
					index++;
					int indexOfSeparator = s.IndexOf(argSeparator);
					
					// Determine the option name be it with a value or a switch
					string optionName = (indexOfSeparator != -1) ?
						s.Substring(1, (indexOfSeparator - 1)) : s.Substring(1);
					
					// Retrieve the associated parameter value if any
					string optionValue = (indexOfSeparator != -1) ?
						s.Substring(indexOfSeparator + 1) : null;
					
					options.Add(optionName, optionValue);
				}
				else {
					break;
				}
			}
			
			nameIndex = index + 1;
		}
		
		protected void ParseNames()
		{
			ICollection<string> temp = new List<string>();
			
			if (nameIndex != -1)
				for (int i = nameIndex; i < args.Length; i++)
					temp.Add(args[i]);
			else
				foreach (string s in args)
					if (s[0] != argId)
						temp.Add(s);
			
			if (temp.Count != 0) {
				names = new string[temp.Count];
				temp.CopyTo(names, 0);
			}
		}
	}
}
