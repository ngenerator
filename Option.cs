// /home/jeremie/TaoParser/NGenerator/Option.cs
// Writtent by jeremie at 17:56 09/06/2007
//
// This file is licensed under the LGPL licence as described in the COPYING file

using System;
using System.IO;

namespace NGenerator
{
	public static class Options
	{
		static bool         isSealed     = false;
		static InputType    inputType   = InputType.SingleFile;
		static UnsafeLevel  unsafeLevel = UnsafeLevel.Safe;
		static StreamWriter output      = null;
		static FileInfo   inputFile;
		
		public static void Seal()
		{
			isSealed = true;
			if (output == null)
				output = new StreamWriter("output.cs");
		}
		
		public static InputType Input {
			get {
				return inputType;
			}
			set {
				if (isSealed)
					return;
				inputType = value;
			}
		}
		
		public static UnsafeLevel Unsafe {
			get {
				return unsafeLevel;
			}
			set {
				if (isSealed)
					return;
				unsafeLevel = value;
			}
		}
		
		public static StreamWriter Output {
			get {
				return output;
			}
			set {
				if (isSealed)
					return;
				output = value;
			}
		}
		
		public static FileInfo InputFile {
			get {
				return inputFile;
			}
			set {
				if (isSealed)
					return;
				inputFile = value;
			}
		}
	}
	
	public enum InputType {
		SingleFile,
		Directory
	}
	
	public enum UnsafeLevel {
		// eg use IntPtr
		Safe,
		// eg use pointer
		Unsafe,
		// eg use pointer internally but give a safe api with IntPtr (take care of the marshalling and all)
		UnsafeWithWrapper
	}
}
