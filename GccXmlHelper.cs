// /home/jeremie/TaoParser/NGenerator/GccXmlHelper.cs
// Writtent by jeremie at 20:02 30/05/2007
//
// This file is licensed under the LGPL licence as described in the COPYING file

using System;
using System.Diagnostics;
using System.IO;

namespace NGenerator
{
	public static class GccXmlHelper
	{
		const string gccXmlProg = "gccxml";
		const string xmlArg = "-fxml=";
		
		static ProcessStartInfo stdInfo = new ProcessStartInfo();
		//static ProcessStartInfo makeInfo = new ProcessStartInfo();
		
		static Process commonProcess = new Process();
		
		static GccXmlHelper()
		{
			stdInfo.WorkingDirectory = Environment.CurrentDirectory;
			stdInfo.FileName = gccXmlProg;
			//makeInfo.FileName = "make";
			//stdInfo.Arguments
		}
		
		/// <summary>
		/// This method is called when the user pass a directory to ngenerator like /home/libfoo/include/*
		/// <argument name="dpath">The path to the directory (e.g. /home/libfoo/include/)</argument>
		/// <return> A FileInfo reference to the generated gccxml's xml output</return>
		public static FileInfo ParseDirectory(string dpath)
		{
			Logger.Debug("Parsing the directory : " + dpath);
			EnsurePathExists(dpath);
			FileInfo finalToParse = ResolveDirectory(dpath);
			return Launch(finalToParse.FullName);
		}
		
		/// <summary>
		/// This method is called when the user pass directly a master .h file
		/// to ngenerator like /home/libfoo/include/All.h
		/// <argument name="filename">The path to the file (e.g. /home/libfoo/include/All.h)</argument>
		/// <return> A FileInfo reference to the generated gccxml's xml output</return>
		public static FileInfo ParseFile(string filename)
		{
			EnsurePathExists(filename);
			FileInfo finalToParse = ResolveFile(filename);
			return Launch(finalToParse.FullName);
		}
		
		static FileInfo Launch(string filename)
		{
			string output = AddSourceAndParametrize(filename);
			StartProcess(stdInfo);
			return new FileInfo(output);
		}
		
		/// <summary>
		/// This method takes the input file, strip unnecessary bits from it (like useless headers)
		/// and return a FileInfo reference to the cleaned file
		/// </summary>
		static FileInfo ResolveFile(string filepath)
		{
			// The end-file should be created in the /tmp/ directory using
			// Path.GetTempPath() or directly with Path.GetTempFile()
			return null;
		}
		
		/// <summary>
		/// This method takes the input directory, strip unnecessary bits from all .h files
		/// contained in it (like useless headers), concatenate what is resting in one temp .h file
		/// and finally returns a FileInfo reference to the cleaned file.
		/// </summary>
		static FileInfo ResolveDirectory (string directoryPath)
		{
			return null;
		}
		
		/*public static FileInfo UseMakeFile(string makeFileName)
		{
			EnsureFileExists(makeFileName);
			string outputFileName = MakeOutputName();
			// Set some environment variable to interact with the makefile
			Environment.SetEnvironmentVariable("CC", gccXmlProg);
			Environment.SetEnvironmentVariable("CFLAGS", xmlArg + outputFileName);
			
			makeInfo.Arguments = "-f " + makeFileName.Trim();
			
			StartProcess(makeInfo);
			
			return new FileInfo(outputFileName);
		}*/
		
		static void StartProcess(ProcessStartInfo info)
		{
			commonProcess.StartInfo = info;
			commonProcess.Start();
			commonProcess.WaitForExit();
			//System.Threading.Thread.Sleep(1000);
		}
		
		static void EnsurePathExists(string path)
		{
			// Need to test file too
			if(!Directory.Exists(Path.GetFullPath(path)))
				throw new DirectoryNotFoundException("The directory supplied doesn't exist : " + path);
		}
		
		static string AddSourceAndParametrize(string filename)
		{
			if (filename == null)
				throw new ArgumentNullException("filename");
			if (filename.Length == 0)
				throw new ArgumentException("The input file is empty", "filename");
			
			System.Text.StringBuilder sb = new System.Text.StringBuilder(filename.Length + 40);
			string output = MakeOutputName();
			sb.Append("-fno-builtin ");
			sb.Append(filename);
			sb.Append(' ');
			sb.Append(xmlArg);
			sb.Append(output);
			
			stdInfo.Arguments = sb.ToString();
			Logger.Debug("Parametrized Process with the following args : " + sb.ToString());
			
			return output;
		}
		
		static string MakeOutputName()
		{
			/*string date = DateTime.Now.ToShortDateString();
			date = date.Replace('/', '-');
			return "output-" + date + ".xml";*/
			return Path.GetTempFileName();
		}
	}
}
