// /home/jeremie/TaoParser/NGenerator/ITranslator.cs
// Writtent by jeremie at 14:19 04/06/2007
//
// This file is licensed under the LGPL licence as described in the COPYING file

using System;
using System.CodeDom;
using System.IO;

namespace NGenerator
{
	public interface ITranslator
	{
		/// <summary>
		/// The fileinfo contain the document (XML or not) which will be used
		/// by the target Translator to build a corresponding CodeDOM tree
		/// </summary>
		CodeCompileUnit Translate(string filename);
	}
}
