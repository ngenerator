// /home/jeremie/TaoParser/NGenerator/GccXmlTranslator.cs
// Writtent by jeremie at 14:21 04/06/2007
//
// This file is licensed under the LGPL licence as described in the COPYING file

using System;
using System.IO;
using System.Collections.Generic;
// TODO: Drop CodeDOM and use a cooler replacement (maybe add an option to autogenerate with Cecil too)
using System.CodeDom;
using System.Xml;
using Cs = CodeScheme;

namespace NGenerator
{
	public sealed class GccXmlTranslator: ITranslator
	{
		CodeCompileUnit cu = new CodeCompileUnit();
		XmlDocument doc = new XmlDocument();
		CodeNamespace mainNs = new CodeNamespace("PInvokeFunctions");
		
		CodeCompileUnit ITranslator.Translate (string filename)
		{
			//doc.Load(document.OpenRead());
			//Console.WriteLine(File.ReadAllText(filename));
			doc.LoadXml(File.ReadAllText(filename));
			Asserter.Assert(doc.ChildNodes.Count > 0, "No nodes in the XML document, WTF ?!");
			
			cu.Namespaces.Add(mainNs);
			mainNs.Imports.Add(new CodeNamespaceImport("System"));
			mainNs.Imports.Add(new CodeNamespaceImport("System.Runtime.InteropServices"));
			mainNs.Imports.Add(new CodeNamespaceImport("System.Security"));
			
			ParseTypedefs();
			ParseStructs();
			ParseEnums();
			ParseFunctions();
				
			return cu;
		}
		
		void ParseTypedefs()
		{
			XmlNodeList list = doc.GetElementsByTagName("Typedef");
			Asserter.Assert(list != null, "The XmlNodeList for typedef is null");
			
			foreach (XmlNode node in list) {
				if (node == null) continue;
				
				XmlNode type = doc.SelectSingleNode("//*[@id='" + node.Attributes["type"].Value + "']");
				
				Asserter.Assert(type != null, "The type XmlNode is null. The id was : " + node.Attributes["type"].Value);
				string typeStr = type.Attributes["name"].Value;
				mainNs.Imports.Add(new CodeNamespaceImport(node.Attributes["name"].Value + "=" + typeStr));
			}
		}
		
		void ParseStructs()
		{
			XmlNodeList list = doc.GetElementsByTagName("Struct");
			Asserter.Assert(list != null, "The XmlNodeList for structs is null");
			
			foreach (XmlNode node in list) {
				// Make the base struct
				CodeTypeDeclaration strct = new CodeTypeDeclaration(node.Attributes["name"].Value);
				strct.IsStruct = true;
				
				// Populate it with fields
				string[] ids = node.Attributes["members"].Value.Split(' ');
/*#if DEBUG
				foreach (string s in ids) {
					System.Console.Write('[' + s + "] ");
				}
#endif*/
				foreach (string id in ids) {
					XmlNode field = doc.SelectSingleNode("//Field[@id='" + id + "']");
					
					Asserter.Assert(field != null, "Fields XmlNode is null");
#if DEBUG
					foreach (XmlAttribute xmlA in field.Attributes) {
						System.Console.WriteLine(xmlA.Name + "  :  " + xmlA.Value);
					}
#endif
					//Asserter.Assert(field["type"].Value != null, "The type attribute of the field is null");
					string strType = null;
					XmlNode fieldType = doc.SelectSingleNode("//*[@id='" + field.Attributes["type"].Value + "']");
					Asserter.Assert(fieldType != null, "fieldType is null");
					
					if (fieldType.Name.Equals("PointerType", StringComparison.OrdinalIgnoreCase) || 
					    fieldType.Name.Equals("ArrayType", StringComparison.OrdinalIgnoreCase)) {
						// TODO: handle double pointer, multi-dimmension array and that kind of stuff
						strType = doc.SelectSingleNode("//*[@id='" + fieldType.Attributes["type"].Value + "']").Attributes["name"].Value + 
							((fieldType.Name.Equals("PointerType", StringComparison.OrdinalIgnoreCase)) ? "*" : "[]");
					} else {
						strType = fieldType.Attributes["name"].Value;
					}
					
					CodeMemberField member = new CodeMemberField(strType,
					                                             field.Attributes["name"].Value);
					
					strct.Members.Add(member);
				}
				mainNs.Types.Add(strct);
			}
		}
		
		void ParseEnums()
		{
			
		}
		
		void ParseFunctions()
		{
			
		}
	}
}
