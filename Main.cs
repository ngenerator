// /home/jeremie/TaoParser/NGenerator/MainClass.cs
// Writtent by jeremie at 21:48 03/06/2007
//
// This file is licensed under the LGPL licence as described in the COPYING file

using System;
using System.IO;
using System.CodeDom;
using System.CodeDom.Compiler;

namespace NGenerator
{	
	public class MainClass
	{
		static CommandLine cmd;
		
		public static void Main(string[] args)
		{
			cmd = new CommandLine(args);
			if (args.Length == 0 || cmd.IsDefined("h"))
			    PrintUsageAndExit();
			string[] names = cmd.Names;
			
			if (names == null || (names != null && names.Length != 1))
				PrintUsageAndExit();
			
			PopulateOptions();
			
			Logger.Debug("CommandLine retrieved and parsed. Processing the files : " + string.Join(", ", names));
			
			FileInfo fi = null;
			
			if (Options.Input == InputType.Directory)
				// TODO changes these things to use Options
				fi = GccXmlHelper.ParseDirectory(Options.InputFile.FullName);
			else
				fi = GccXmlHelper.ParseFile(Options.InputFile.FullName);
			
			string filename = fi.FullName;
			Logger.Debug("File generated : " + filename + ". Does it exists ? : " + fi.Exists.ToString());
			
			ITranslator trans = new GccXmlTranslator();
			CodeCompileUnit cu = trans.Translate(filename);
			CodeDomProvider cp = new Microsoft.CSharp.CSharpCodeProvider();
			cp.GenerateCodeFromCompileUnit(cu, Options.Output, new CodeGeneratorOptions());
		}
		
		static void PrintUsageAndExit()
		{
			string usage = "NGenerator v0.1 (c) 2007 LAVAL Jérémie -- Generate *clean* C# P/Invoke class and method for interoperating with C libraries\n\n" +
				"Usage is : ngenerator <options> filepath|directorypath\nWhere <options> may be :\n\n" +
					"\t-h : print this help notice\n" + "\t-d : Tell NGenerator that the supplied path is a directory path" + 
					"\t-o filename : write the source code in filename\n\t-u Unsafe|Safe[UnsafeWithWrapper : the degree of \"unsafeness\" of the source, see man";
			
			Console.WriteLine(usage);
			
			Environment.Exit(0);
		}
		
		static void PopulateOptions()
		{
			Options.InputFile = new FileInfo(cmd.Names[0]);
			Options.Input = (cmd.IsDefined("d")) ? InputType.Directory : InputType.SingleFile;
			Options.Output = (cmd.IsDefined("o")) ? new StreamWriter(cmd["o"]) : null;
			// TODO: make a cool Enum.(Try)Parse method instead of the funny thing that is Enum.Parse()
			Options.Unsafe = (cmd["u"].Equals("Unsafe", StringComparison.OrdinalIgnoreCase)) ? UnsafeLevel.Unsafe : UnsafeLevel.Safe;
			
			// Now Options is read-only
			Options.Seal();
		}
	}
}
