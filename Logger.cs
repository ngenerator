// /home/jeremie/TaoParser/NGenerator/Logger.cs
// Writtent by jeremie at 17:47 09/06/2007
//
// This file is licensed under the LGPL licence as described in the COPYING file

using System;
using System.IO;
using System.Diagnostics;

namespace NGenerator
{
	public enum LogType {
		Debug,
		Error
	}
	
	public static class Logger
	{
		static TextWriter sw = Console.Out;
		
		public static TextWriter Output {
            get  {
                return sw;
            }
			set {
				if (value == null)
					throw new ArgumentNullException("value");
                sw.Dispose();
				sw = value;
			}
		}
		
		private static void Log(LogType type, string mess)
		{
			StackTrace st = new StackTrace();
			StackFrame sf = st.GetFrame(2);
			
			if (mess == null || sf == null)
				return;
			
			if (sw == Console.Out)
				Console.ForegroundColor = (type == LogType.Error) ? ConsoleColor.DarkRed : ConsoleColor.DarkBlue;
			
			sw.WriteLine(type.ToString()+" -> "+ sf.GetMethod().Name+" :: "+mess);
			
			if (sw == Console.Out)
				Console.ResetColor();
		}
		
		[System.Diagnostics.ConditionalAttribute("__DEBUG__")]
		public static void Debug(string mess)
		{
			Log(LogType.Debug, mess);
		}
		
		public static void Error(string mess, Exception ex)
		{
			string message = (ex == null) ? mess : (mess + ", Exception type : " + ex.GetType().ToString()
				+ ", Exception message : " + ex.Message);
			Log(LogType.Error, message);
		}
	}
}
