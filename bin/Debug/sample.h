#define SOME_CONSTANT 0x001

typedef int GLint;

typedef enum {
	foo,
	bar,
	prout
} CaseFoldMapping;

void add(GLint a, int b);

void substract(float a, float b);

typedef struct Sample
{
	int foo;
	char * bar;
	long foobar[];
	long ** prout;
	Sample* next;
} Sample;

int case_fold_005[][7] = {
    { 0x0104, 0x0105, 0x0000, 0x0000 },
    { 0x0401, 0x0451, 0x0000, 0x0000 },
    { 0x0500, 0x0501, 0x0000, 0x0000 },
    { 0x1F1A, 0x1F12, 0x0000, 0x0000 },
    { 0x2C29, 0x2C59, 0x0000, 0x0000 },
    { 0x10401, 0x10429, 0x0000, 0x0000 }
};

void dosomething(Sample bar);
